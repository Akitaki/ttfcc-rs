#[macro_export]
macro_rules! fvec3 {
    [$x: expr, $y: expr, $z: expr] => {
        glm::vec3($x as f32, $y as f32, $z as f32)
    };
}

#[macro_export]
macro_rules! fvec2 {
    [$x: expr, $y: expr] => {
        glm::vec2($x as f32, $y as f32)
    };
}
