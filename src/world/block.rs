use super::{Face, LocalBlockPos, LocalPos};
use crate::render::{BlockRenderBuffer, BlockVertex};

#[repr(u8)]
#[derive(Debug, Copy, Clone)]
pub enum Block {
    Air = 0,
    Stone = 1,
    RenderNone = 255,
}

pub struct AdjBlocks {
    pub(super) east: Option<Block>,
    pub(super) west: Option<Block>,
    pub(super) south: Option<Block>,
    pub(super) north: Option<Block>,
    pub(super) top: Option<Block>,
    pub(super) bottom: Option<Block>,
}

impl AdjBlocks {
    pub fn on_face(&self, face: Face) -> Option<Block> {
        use Face::*;
        match face {
            East => self.east,
            West => self.west,
            South => self.south,
            North => self.north,
            Top => self.top,
            Bottom => self.bottom,
        }
    }
}

impl Block {
    pub fn render(
        &self,
        render_buffer: &mut BlockRenderBuffer,
        block_pos: LocalBlockPos,
        adj: AdjBlocks,
    ) {
        if matches!(self, Block::Air | Block::RenderNone) {
            return;
        }

        static FACE_VERTICES: [[(LocalPos, [f32; 2]); 4]; 6] = [
            [
                (LocalPos::new(1.0, 1.0, 1.0), [0.0, 1.0]),
                (LocalPos::new(1.0, 0.0, 1.0), [0.0, 0.0]),
                (LocalPos::new(1.0, 0.0, 0.0), [1.0, 0.0]),
                (LocalPos::new(1.0, 1.0, 0.0), [1.0, 1.0]),
            ],
            [
                (LocalPos::new(0.0, 1.0, 0.0), [0.0, 1.0]),
                (LocalPos::new(0.0, 0.0, 0.0), [0.0, 0.0]),
                (LocalPos::new(0.0, 0.0, 1.0), [1.0, 0.0]),
                (LocalPos::new(0.0, 1.0, 1.0), [1.0, 1.0]),
            ],
            [
                (LocalPos::new(0.0, 1.0, 0.0), [0.0, 1.0]),
                (LocalPos::new(0.0, 1.0, 1.0), [0.0, 0.0]),
                (LocalPos::new(1.0, 1.0, 1.0), [1.0, 0.0]),
                (LocalPos::new(1.0, 1.0, 0.0), [1.0, 1.0]),
            ],
            [
                (LocalPos::new(0.0, 0.0, 1.0), [0.0, 1.0]),
                (LocalPos::new(0.0, 0.0, 0.0), [0.0, 0.0]),
                (LocalPos::new(1.0, 0.0, 0.0), [1.0, 0.0]),
                (LocalPos::new(1.0, 0.0, 1.0), [1.0, 1.0]),
            ],
            [
                (LocalPos::new(0.0, 1.0, 1.0), [0.0, 1.0]),
                (LocalPos::new(0.0, 0.0, 1.0), [0.0, 0.0]),
                (LocalPos::new(1.0, 0.0, 1.0), [1.0, 0.0]),
                (LocalPos::new(1.0, 1.0, 1.0), [1.0, 1.0]),
            ],
            [
                (LocalPos::new(1.0, 1.0, 0.0), [0.0, 1.0]),
                (LocalPos::new(1.0, 0.0, 0.0), [0.0, 0.0]),
                (LocalPos::new(0.0, 0.0, 0.0), [1.0, 0.0]),
                (LocalPos::new(0.0, 1.0, 0.0), [1.0, 1.0]),
            ],
        ];

        static FACE_INDICES: [u32; 6] = [0, 1, 2, 2, 3, 0];

        static FACES: [Face; 6] = [
            Face::East,
            Face::West,
            Face::Top,
            Face::Bottom,
            Face::South,
            Face::North,
        ];

        for &face in FACES.iter() {
            let neighbor = adj.on_face(face);

            match neighbor {
                Some(Block::Air) | Some(Block::RenderNone) => (),
                _ => continue,
            }

            let face_vertices = FACE_VERTICES[face.as_index()];

            // For each point in the face
            for (inblock_pos, face_pos) in face_vertices.iter() {
                let pos = (Into::<LocalPos>::into(block_pos)
                    + inblock_pos.clone())
                .into();
                render_buffer.push_vertice(BlockVertex {
                    pos,
                    face_pos: face_pos.clone(),
                    texture_id: 1,
                })
            }

            let indice_offset = match render_buffer.get_last_indice() {
                Some(last_indice) => last_indice + 4,
                None => 0,
            };
            for indice in FACE_INDICES.iter() {
                render_buffer.push_indice(indice + indice_offset);
            }
        }
    }
}
