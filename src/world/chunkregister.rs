use std::cell::{RefCell, RefMut};
use std::collections::{HashMap, HashSet};
use std::sync::mpsc;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;

use super::{AdjChunks, Chunk, ChunkPopulator, ChunkPos, Direction};

pub struct ChunkRegister {
    chunks: HashMap<ChunkPos, RefCell<Chunk>>,
    loader: ChunkLoader,
    unloader: ChunkUnloader,
    load_pending: HashSet<ChunkPos>,
}

impl ChunkRegister {
    pub fn new() -> Self {
        Self {
            chunks: HashMap::new(),
            loader: ChunkLoader::new(),
            unloader: ChunkUnloader::new(),
            load_pending: HashSet::new(),
        }
    }

    pub fn perform_chunk_io(&mut self, camera_pos: glm::Vec3) {
        struct Center {
            p: i64,
            q: i64,
        };
        let center = Center {
            p: (camera_pos[0] as i64).div_euclid(16),
            q: (camera_pos[2] as i64).div_euclid(16),
        };
        const RADIUS: i64 = 10;

        let nearby_chunks_iter = iproduct!(
            (center.p - RADIUS - 1)..(center.p + RADIUS + 1),
            (center.q - RADIUS - 1)..(center.q + RADIUS + 1)
        )
        .filter_map(|(p, q)| {
            let dist_sqr =
                ((p - center.p).pow(2) + (q - center.q).pow(2)) as f64;
            let dist = dist_sqr.sqrt().round() as i64;
            if dist <= RADIUS {
                Some(ChunkPos::new(p, q))
            } else {
                None
            }
        });

        /* Pull loaded chunks from the loader */
        for chunk in self.loader.try_iter() {
            let pos = chunk.get_pos();
            self.load_pending.remove(&pos);

            if let Some(_) = self.chunks.insert(pos, RefCell::new(chunk)) {
                eprintln!("Chunk is repeatedly loaded at {:?}", pos);
            }

            // Mark adjacent chunks as re-render needed
            for pos in pos.neighbors() {
                if let Some(chunk) = self.chunks.get(&pos) {
                    chunk.borrow_mut().mark_need_render(true);
                }
            }
        }

        /* Unload loaded chunks that are not in radius and load chunks in radius */

        let nearby_chunkpos_set: HashSet<_> = nearby_chunks_iter.collect();
        let loaded_chunkpos_set: HashSet<_> =
            self.chunks.keys().cloned().collect();

        // Send load messages
        let chunkpos_to_load =
            nearby_chunkpos_set.difference(&loaded_chunkpos_set);
        for &pos in chunkpos_to_load {
            // Add this pos to pending set. `false` is returned if it's already there.
            // This if-stmt avoids repeated load requests.
            if self.load_pending.insert(pos) {
                // Send
                if let Err(e) = self.loader.send_load_chunk(pos) {
                    eprintln!("Failed to send load chunk: {:?}", e);
                }
            }
        }

        // Send unload messages
        let chunkpos_to_unload =
            loaded_chunkpos_set.difference(&nearby_chunkpos_set);
        for &pos in chunkpos_to_unload {
            let chunk = self.chunks.remove(&pos);
            if let Err(e) = chunk
                .ok_or_else(|| panic!("Failed to remove chunk from chunks"))
                .map(RefCell::<Chunk>::into_inner)
                .map(|chunk| self.unloader.send_unload_chunk(chunk))
            {
                eprintln!("Failed to send unload chunk: {:?}", e);
            }
        }
    }

    /// Load chunk at `pos` into memory if not yet.
    /// The chunk will be populated.
    #[cfg(feature = "disabled")]
    pub fn load_chunk<P>(&mut self, pos: P) -> &RefCell<Chunk>
    where
        P: Into<ChunkPos>,
    {
        let pos = pos.into();
        let ref mut chunks = self.chunks;
        let ref mut populator = self.populator;

        chunks.entry(pos).or_insert_with(|| {
            // Create new chunk instance and populate with content
            let mut chunk = Chunk::new(pos);
            populator.populate(&mut chunk);
            RefCell::new(chunk)
        })
    }

    pub fn unload_chunk_at<P>(&mut self, pos: P)
    where
        P: Into<ChunkPos>,
    {
        let pos = pos.into();
        if let Some(Err(e)) = (self.chunks)
            .remove(&pos)
            .map(|chunk| chunk.into_inner())
            .map(|chunk| self.unloader.send_unload_chunk(chunk))
        {
            println!("Failed to send unload chunk: {:?}", e);
        }
    }

    pub fn adj_iter_mut(&self) -> ChunksAdjIterMut {
        ChunksAdjIterMut::new(self)
    }

    pub fn iter_mut(&self) -> ChunksIterMut {
        ChunksIterMut::new(self)
    }
}

pub struct ChunksIterMut<'a> {
    chunk_iter:
        std::collections::hash_map::Values<'a, ChunkPos, RefCell<Chunk>>,
}

impl<'a> ChunksIterMut<'a> {
    pub fn new(chunkreg: &'a ChunkRegister) -> Self {
        Self {
            chunk_iter: chunkreg.chunks.values(),
        }
    }
}

impl<'a> Iterator for ChunksIterMut<'a> {
    type Item = RefMut<'a, Chunk>;
    fn next(&mut self) -> Option<Self::Item> {
        self.chunk_iter.next().map(RefCell::borrow_mut)
    }
}

pub struct ChunksAdjIterMut<'a> {
    chunkreg: &'a ChunkRegister,
    chunk_keys_iter: std::vec::IntoIter<ChunkPos>,
}

impl<'a> ChunksAdjIterMut<'a> {
    pub fn new(chunkreg: &'a ChunkRegister) -> Self {
        let chunk_keys: Vec<_> = chunkreg.chunks.keys().cloned().collect();
        Self {
            chunkreg,
            chunk_keys_iter: chunk_keys.into_iter(),
        }
    }
}

impl<'a> Iterator for ChunksAdjIterMut<'a> {
    type Item = (RefMut<'a, Chunk>, AdjChunks<'a>);
    fn next(&mut self) -> Option<Self::Item> {
        use Direction::*;

        let ref chunkreg = self.chunkreg;
        let ref mut chunk_keys_iter = self.chunk_keys_iter;

        chunk_keys_iter.next().map(|pos| {
            let chunk = chunkreg.chunks.get(&pos).expect("Failed to get chunk");

            // Get `Ref` to adjacent chunk on direction `dir`
            let try_borrow_chunk = |dir| {
                chunkreg
                    .chunks
                    .get(&pos.neighbor_on_dir(dir))
                    .map(|chunk| chunk.borrow())
            };

            let adj_chunks = AdjChunks {
                east: try_borrow_chunk(East),
                west: try_borrow_chunk(West),
                south: try_borrow_chunk(South),
                north: try_borrow_chunk(North),
            };

            (chunk.borrow_mut(), adj_chunks)
        })
    }
}

struct ChunkLoader {
    join_handle: Option<std::thread::JoinHandle<()>>,
    sender: Sender<ChunkLoaderMsg>,
    loaded_receiver: Receiver<Chunk>,
}

enum ChunkLoaderMsg {
    Load(ChunkPos),
    Exit,
}

impl ChunkLoader {
    pub fn new() -> Self {
        let (sender, receiver) = channel();
        let (loaded_sender, loaded_receiver) = channel();
        Self {
            join_handle: Some(std::thread::spawn(move || {
                Self::load_loop(receiver, loaded_sender)
            })),
            sender,
            loaded_receiver,
        }
    }

    pub fn load_loop(
        receiver: Receiver<ChunkLoaderMsg>,
        loaded_sender: Sender<Chunk>,
    ) {
        use ChunkLoaderMsg::*;

        const LOADER_SLEEP_MS: u64 = 10;

        let populator = ChunkPopulator::new(0);
        while let Ok(msg) = receiver.recv() {
            match msg {
                Load(pos) => {
                    let mut chunk = Chunk::new(pos);
                    populator.populate(&mut chunk);
                    // println!("Populated {:?}", pos);
                    if let Err(e) = loaded_sender.send(chunk) {
                        eprintln!("Failed to send loaded chunk back: {:?}", e);
                    }
                }
                Exit => break,
            }
            thread::sleep(std::time::Duration::from_millis(LOADER_SLEEP_MS));
        }
    }

    /// `try_iter` of the underlying loaded-chunk receiver
    pub fn try_iter(&self) -> mpsc::TryIter<Chunk> {
        self.loaded_receiver.try_iter()
    }

    pub fn send_load_chunk(&mut self, pos: ChunkPos) -> Result<(), Error> {
        self.sender.send(ChunkLoaderMsg::Load(pos))?;
        Ok(())
    }
}

impl Drop for ChunkLoader {
    fn drop(&mut self) {
        self.sender
            .send(ChunkLoaderMsg::Exit)
            .expect("Failed to send exit to ChunkUnloader");
        self.join_handle.take().map(thread::JoinHandle::join);
    }
}

struct ChunkUnloader {
    join_handle: Option<thread::JoinHandle<()>>,
    sender: mpsc::Sender<ChunkUnloaderMsg>,
}

enum ChunkUnloaderMsg {
    Unload(Chunk),
    Exit,
}

impl ChunkUnloader {
    pub fn new() -> Self {
        let (sender, receiver) = channel();
        Self {
            join_handle: Some(thread::spawn(move || {
                Self::unload_loop(receiver)
            })),
            sender,
        }
    }

    pub fn unload_loop(receiver: Receiver<ChunkUnloaderMsg>) {
        use ChunkUnloaderMsg::*;
        while let Ok(msg) = receiver.recv() {
            match msg {
                Unload(chunk) => println!("Unloaded {:?}", chunk.get_pos()),
                Exit => break,
            }
        }
    }

    pub fn send_unload_chunk(&mut self, chunk: Chunk) -> Result<(), Error> {
        self.sender.send(ChunkUnloaderMsg::Unload(chunk))?;
        Ok(())
    }
}

impl Drop for ChunkUnloader {
    fn drop(&mut self) {
        self.sender
            .send(ChunkUnloaderMsg::Exit)
            .expect("Failed to send exit to ChunkUnloader");
        self.join_handle.take().map(thread::JoinHandle::join);
    }
}

#[derive(Debug)]
enum Error {
    LoadSend(mpsc::SendError<ChunkLoaderMsg>),
    UnloadSend(mpsc::SendError<ChunkUnloaderMsg>),
}

impl From<mpsc::SendError<ChunkLoaderMsg>> for Error {
    fn from(e: mpsc::SendError<ChunkLoaderMsg>) -> Self {
        Self::LoadSend(e)
    }
}

impl From<mpsc::SendError<ChunkUnloaderMsg>> for Error {
    fn from(e: mpsc::SendError<ChunkUnloaderMsg>) -> Self {
        Self::UnloadSend(e)
    }
}
