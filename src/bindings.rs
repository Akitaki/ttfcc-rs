use glutin::event::{KeyboardInput, VirtualKeyCode};

pub struct Binding {
    pressed: bool,
}

impl Binding {
    pub fn new() -> Self {
        Self { pressed: false }
    }

    pub fn set_pressed(&mut self, pressed: bool) {
        self.pressed = pressed
    }

    pub fn is_pressed(&self) -> bool {
        self.pressed
    }

    pub fn run_if_pressed<F>(&self, callback: F)
    where
        F: FnOnce(),
    {
        if self.is_pressed() {
            callback()
        }
    }
}

pub struct Bindings {
    pub jump: Binding,
    pub sneak: Binding,
    pub forward: Binding,
    pub backward: Binding,
    pub left: Binding,
    pub right: Binding,
}

impl Bindings {
    /// Build with the default bindings
    pub fn default() -> Self {
        Self {
            jump: Binding::new(),
            sneak: Binding::new(),
            forward: Binding::new(),
            backward: Binding::new(),
            left: Binding::new(),
            right: Binding::new(),
        }
    }

    pub fn update(&mut self, event: KeyboardInput) {
        if let KeyboardInput {
            virtual_keycode: Some(keycode),
            state,
            ..
        } = event
        {
            let binding = match keycode {
                VirtualKeyCode::Space => Some(&mut self.jump),
                VirtualKeyCode::LShift => Some(&mut self.sneak),
                VirtualKeyCode::W => Some(&mut self.forward),
                VirtualKeyCode::S => Some(&mut self.backward),
                VirtualKeyCode::A => Some(&mut self.left),
                VirtualKeyCode::D => Some(&mut self.right),
                _ => None,
            };
            let pressed = match state {
                glutin::event::ElementState::Pressed => true,
                glutin::event::ElementState::Released => false,
            };
            binding.map(|binding| binding.set_pressed(pressed));
        }
    }
}
