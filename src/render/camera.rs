/// Camera struct taken from old codebase

pub struct Camera {
    // Camera position
    pos: glm::Vec3,
    // Pitch and yaw in degrees
    pitch: f32,
    yaw: f32,
    front: glm::Vec3,
}

impl Camera {
    const KEY_SENSITIVITY: f32 = 0.1;
    const CURSOR_SENSITIVITY: f32 = 0.1;

    pub fn new() -> Self {
        Self {
            pos: glm::vec3(-3.0, 1.5, 0.0),
            pitch: 0.0,
            yaw: 0.0,
            front: glm::vec3(1.0, 0.0, 0.0),
        }
    }

    pub fn handle_cursur_offset(&mut self, (dx, dy): (f32, f32)) {
        // Offset pitch and yaw
        self.pitch -= dy * Self::CURSOR_SENSITIVITY;
        self.yaw += dx * Self::CURSOR_SENSITIVITY;

        // Clamp pitch between -90 to 90 degrees
        self.pitch = glm::clamp_scalar(self.pitch, -89.99, 89.99);
    }

    pub fn handle_forward(&mut self, frame_elapsed: time::Duration) {
        let millisec = frame_elapsed.whole_milliseconds();
        self.pos +=
            self.front * millisec as f32 * Self::KEY_SENSITIVITY * 0.001;
    }

    pub fn handle_backward(&mut self, frame_elapsed: time::Duration) {
        let millisec = frame_elapsed.whole_milliseconds();
        self.pos -=
            self.front * millisec as f32 * Self::KEY_SENSITIVITY * 0.001;
    }

    pub fn handle_right(&mut self, frame_elapsed: time::Duration) {
        let millisec = frame_elapsed.whole_milliseconds();
        let right = glm::cross(&self.front, &glm::vec3(0.0, 1.0, 0.0));
        self.pos += right * millisec as f32 * Self::KEY_SENSITIVITY * 0.001;
    }

    pub fn handle_left(&mut self, frame_elapsed: time::Duration) {
        let millisec = frame_elapsed.whole_milliseconds();
        let right = glm::cross(&self.front, &glm::vec3(0.0, 1.0, 0.0));
        self.pos -= right * millisec as f32 * Self::KEY_SENSITIVITY * 0.001;
    }

    pub fn handle_fly(&mut self, frame_elapsed: time::Duration) {
        let millisec = frame_elapsed.whole_milliseconds();
        self.pos += glm::vec3(0.0, 1.0, 0.0)
            * millisec as f32
            * Self::KEY_SENSITIVITY
            * 0.001;
    }

    pub fn handle_sneak(&mut self, frame_elapsed: time::Duration) {
        let millisec = frame_elapsed.whole_milliseconds();
        self.pos -= glm::vec3(0.0, 1.0, 0.0)
            * millisec as f32
            * Self::KEY_SENSITIVITY
            * 0.001;
    }

    fn update_front(&mut self) {
        let front_x =
            self.yaw.to_radians().cos() * self.pitch.to_radians().cos();
        let front_y = self.pitch.to_radians().sin();
        let front_z =
            self.yaw.to_radians().sin() * self.pitch.to_radians().cos();

        self.front = glm::vec3(front_x, front_y, front_z);
    }

    pub fn get_view_matrix(&mut self) -> glm::Mat4 {
        self.update_front();
        glm::look_at(
            &self.pos,
            &(self.pos + self.front),
            &glm::vec3(0.0, 1.0, 0.0),
        )
    }

    pub fn get_pos(&self) -> glm::Vec3 {
        self.pos
    }
}
